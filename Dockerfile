FROM debian:bullseye

ARG LUFI_BUILD_VERSION
ENV APP_HOME=/lufi

# Install dependencies
RUN apt-get update \
&& apt-get -yq install build-essential libssl-dev libio-socket-ssl-perl liblwp-protocol-https-perl libpq-dev zlib1g-dev postgresql-client git wget unzip \
&& apt-get clean && cpan Carton

# Install Supercronic to run cron jobs
# (delete IP, delete old files...)
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.9/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=5ddf8ea26b56d4a7ff6faecdd8966610d5cb9d85

RUN wget "$SUPERCRONIC_URL" \
    && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
    && chmod +x "$SUPERCRONIC" \
    && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
    && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

# Get Lufi release
RUN git clone -b ${LUFI_BUILD_VERSION} --depth 1 https://framagit.org/fiat-tux/hat-softwares/lufi.git

WORKDIR ${APP_HOME}

# Install Lufi's dependencies
RUN carton install --deployment --without=test --without=sqlite --without=mysql

# Add custom theme
COPY picadrop ${APP_HOME}/themes/picadrop

# Add custom entrypoint
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 8081

CMD ["carton", "exec", "hypnotoad", "-f", "/lufi/script/lufi"]
