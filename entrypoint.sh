#!/bin/bash

if [ -z "${EMAIL_PASSWORD}" ]; then
  echo "EMAIL_PASSWORD not set, exiting!"
  exit 1
fi

if [ -z "${POSTGRES_DB}" ]; then
  echo "POSTGRES_DB not set, exiting!"
  exit 1
fi

if [ -z "${POSTGRES_USER}" ]; then
  echo "POSTGRES_USER not set, exiting!"
  exit 1
fi

if [ -z "${POSTGRES_PASSWORD}" ]; then
  echo "POSTGRES_PASSWORD not set, exiting!"
  exit 1
fi

echo "Create crontab for cleaning tasks..."

# See https://framagit.org/fiat-tux/hat-softwares/lufi/-/wikis/cron-jobs
# Delete the IP addresses of file's senders after a configurable delay
echo "0 0 * * * carton exec $APP_HOME/script/lufi cron cleanbdd --mode production" > /crontab.conf
# Delete files which availability delay is over
echo "0 0 * * * carton exec $APP_HOME/script/lufi cron cleanfiles --mode production" > /crontab.conf

# Wait database to be ready
while ! PGPASSWORD="${POSTGRES_PASSWORD}" psql -h"lufidb" -U"${POSTGRES_USER}" -d"${POSTGRES_DB}" -c "SELECT 1" &>/dev/null; do
		echo "Database server not ready yet, re-trying in 5 seconds..."
    sleep 5
done

echo "Start Lufi..."
$@ &

echo "Start supercronic..."
supercronic /crontab.conf
